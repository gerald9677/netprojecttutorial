﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleado : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleado;
        public FrmGestionEmpleado()
        {
            InitializeComponent();
            bsEmpleado = new BindingSource();
        }

        public DataSet DsEmpleados
        {
            set
            {
                dsEmpleados = value;
            }
        }

        private void FrmGestionEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleado.DataSource = dsEmpleados;
            bsEmpleado.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleado.DataSource = bsEmpleado;
            dgvEmpleado.AutoGenerateColumns = true;
        }
    }
}
