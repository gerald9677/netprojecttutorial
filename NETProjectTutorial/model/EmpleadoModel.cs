﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados = new List<Empleado>();

        public static List<Empleado> GetLstEmpleado()
        {
            return Lstempleados;
        }

        public static void populate()
        {
            Empleado[] empleados =
            {
                new Empleado(1,"1658574-2","001-110288-5588D","Pepito","Perez","del arbolito 2c. abajo","22558877","88774455",Empleado.Sexo.MASCULINO,25145.33),
                new Empleado(2,"","","","","","","",Empleado.Sexo.FEMENINO,12345),
                new Empleado(3,"","","","","","","",Empleado.Sexo.MASCULINO,12345)
            };

            Lstempleados = empleados.ToList();
        }
        


    }
}
